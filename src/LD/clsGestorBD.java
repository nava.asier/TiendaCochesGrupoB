package LD;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;

import COMUN.itfData;
import LN.clsCliente;
import LN.clsCoche;

public class clsGestorBD {

	private static final String URL="jdbc:mysql://localhost:3306/";
	private static final String SCHEMA = "TiendaCoches";
	private static final String PARAMS="?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC&useSSL=false";
	private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
	private static final String USER = "root";
	private static final String PASS = "root";
	
	
	
	/**
	 * Objeto para crear la conexi�n a base de datos.
	 */
	Connection conn = null;
	
	/**
	 * Objeto para crear la consulta a base de datos.
	 */
	PreparedStatement ps=null;
	
	/**
	 * Objeto para devolver el resultado de la consulta.
	 */
	ResultSet rs=null;
	
	
	
	/**
	 * M�todo para la conexi�n a la base de datos.
	 * 
	 */
	 public void Connect()
	 {
		 
		 
		 try 
		 {
			//Se carga el driver de acceso a datos
		   Class.forName(DRIVER);
		   conn = DriverManager.getConnection(URL+SCHEMA+PARAMS,USER,PASS);
		   System.out.println("Connected to the database");
		   
		   
		 }
		 catch (Exception e) 
		 {
		     System.out.println("NO CONNECTION ");
		     System.out.println(e);
		     
		 }
	 }
	 
	 
	 public void Disconnect()
	 {
		 
		 try 
		 {
			conn.close();
			ps.close(); // cerrar el statement tb cierra el resultset.
		 } 
		 catch (SQLException e) 
		 {
			
		 }
		 finally 
		 {
			 try {conn.close();} catch(Exception e){/*no hago nada*/}
			 try {ps.close();} catch(Exception e){/*no hago nada*/}
		 }
		 
		 
	 }
	 
	 
	 
	 private void CompletarParametrosQuery(ArrayList<Object> prop)
	 {
		 int cont=1;
		 for(Object param : prop)
		 {
			 try {
		    		if (param instanceof String)
		    		{
		    			
						ps.setString(cont, (String)param);
						cont++;
		    			continue;
		    		}
		    			
		    		if (param instanceof Integer)
		    		{
			    			ps.setInt(cont, (Integer)param);
			    			cont++;
			    			continue;
		    		}
		    		
		    		if (param instanceof java.time.LocalDate)
		    		{	
//			    			java.sql.Date fecha=(java.sql.Date)param;
		    				java.sql.Date fecha=java.sql.Date.valueOf((java.time.LocalDate)param);
		    				ps.setDate(cont, (java.sql.Date)fecha);
		    				cont++;
			    			continue;
		    		}
		    		
		    		if (param instanceof Float)
		    		{
			    			ps.setFloat(cont, (Float)param);
			    			cont++;
			    			continue;
		    		}
		    		
		    		if (param instanceof Double)
		    		{
			    			ps.setDouble(cont, (Double)param);
			    			cont++;
			    			continue;
		    		}
		    		
		    		if (param instanceof Boolean)
		    		{
			    			ps.setBoolean(cont, (Boolean)param);
			    			cont++;
			    			continue;
		    		}
		    		
		    		if (param instanceof Character)
		    		{
			    			ps.setString(cont, ((Character)param).toString());
			    			cont++;
			    			continue;
		    		}
			 	}
			 	catch (Exception e)
			 	{
			 		System.out.println("Error en la introduccion del parametro: " + cont + " " + e.toString());
			 	}
			 
			}	
		 
	 }
	 
	 /**
	  * Insert de datos. Es general para toda las clases.
	  * @param sql
	  * @param prop
	  */
	 
	 
	 public void InsertDatos (String sql,ArrayList<Object> prop) {
			
			
         try {
        	 
                    
        	      ps = conn.prepareStatement(sql);
                  
                  this.CompletarParametrosQuery(prop);
  

               // execute insert SQL stetement
                  ps.executeUpdate();

                 System.out.println("Record is inserted into DBUSER table!");

         } catch (SQLException e) {

  System.out.println(e.getMessage());

         }
 }
	 /** 
	  * Esta funci�n consiste en una recuperaci�n de datos en la BD, para poder gestionar adecuadamente la autoincrementalidad.
	  * @param tabla
	  * @param id
	  * @return
	  */
	 
		public int DevuelveUltimoId1(String tabla, String id) {


			String sql_devolucion = "select max("+ id + ") from "+ tabla;
			int ult_id = 0;
			
			try 
			{
				 Statement stmt = conn.createStatement();
				 ResultSet result = stmt.executeQuery(sql_devolucion);
			
				 while (result.next()) {	 
					 
					 ult_id = result.getInt(1);
				 }
				 
								
			} 
			catch (SQLException e) 
			{
				System.out.println("Error en la recuperaci�n de datos para SQL= " + sql_devolucion + " " + e);
			}
			
			int act_id = ult_id + 1;
			System.out.println(act_id);
			return act_id;
			
		}
	 /**
	  * 
	  * Busqueda del cliente mediante el DNI 
	  */

	 
	  public ArrayList<itfData> BuscarCliente(String dni) 
	  {
		  
		  
		  String sql_select_venta_electrodomesticos = "SELECT * FROM cliente WHERE dni = '"+ dni + "'";
		  ArrayList<itfData>resultado;
		  resultado=new ArrayList <itfData>();

		  try
		  {
		    	ps = conn.prepareStatement(sql_select_venta_electrodomesticos);

	         rs=ps.executeQuery(sql_select_venta_electrodomesticos);
	         ResultSetMetaData rsmd = rs.getMetaData();
			
		

	  while(rs.next())
	  {
	         clsFila fila = new clsFila();
	         for(int i=1; i<=rsmd.getColumnCount();i++)
	     {

	         fila.ponerColumna(i, rs.getObject(i));
	     }
	  resultado.add(fila);

	  }

	  } 
	  catch (SQLException e) 
	  {
	         System.out.println("Error en la recuperaci�n de datos para SQL= " + sql_select_venta_electrodomesticos + " " + e);
	  }


	  return resultado;
	  }
	  
	  /**
	   * Busqueda de la sucursal mediante la ciudad
	   */
	  
	  public ArrayList<itfData> BuscarSucursalByCiudad(String ciudad) 
	  {
		  
		  
		  String sql_select_venta_electrodomesticos = "SELECT * FROM sucursal WHERE ciudad = '"+ ciudad + "'";
		  ArrayList<itfData>resultado;
		  resultado=new ArrayList <itfData>();

		  try
		  {
		    	ps = conn.prepareStatement(sql_select_venta_electrodomesticos);

	         rs=ps.executeQuery(sql_select_venta_electrodomesticos);
	         ResultSetMetaData rsmd = rs.getMetaData();
			
		

	  while(rs.next())
	  {
	         clsFila fila = new clsFila();
	         for(int i=1; i<=rsmd.getColumnCount();i++)
	     {

	         fila.ponerColumna(i, rs.getObject(i));
	     }
	  resultado.add(fila);

	  }

	  } 
	  catch (SQLException e) 
	  {
	         System.out.println("Error en la recuperaci�n de datos para SQL= " + sql_select_venta_electrodomesticos + " " + e);
	  }


	  return resultado;
	  }
	  
	  /**
	   * Metodo de recuperacion de usuarios y contrase�as.
	   * @param username
	   * @return
	   */
	  
	  public ArrayList<itfData> DevuelvePassword(String username) {
		  

		  String sql_select_venta_electrodomesticos = "SELECT * FROM acceso WHERE username = '"+ username + "'";
		  ArrayList<itfData> resultado;
		  resultado = new ArrayList<itfData>();

		  try
		  {
		    	ps = conn.prepareStatement(sql_select_venta_electrodomesticos);

	         rs=ps.executeQuery(sql_select_venta_electrodomesticos);
	         ResultSetMetaData rsmd = rs.getMetaData();
			
		

	  while(rs.next())
	  {
	         clsFila fila = new clsFila();
	         for(int i=1; i<=rsmd.getColumnCount();i++)
	     {

	         fila.ponerColumna(i, rs.getObject(i));
	     }
	  resultado.add(fila);

	  }

	  } 
	  catch (SQLException e) 
	  {
	         System.out.println("Error en la recuperaci�n de datos para SQL= " + sql_select_venta_electrodomesticos + " " + e);
	  }


	  return resultado;
	  
	  }
	  
	  
	  public ArrayList<itfData> BuscarMarcaByNombre(String nombre) 
	  {
		  
		  
		  String sql_select_marca_nombre = "SELECT * FROM marca WHERE nombre_marca = '"+ nombre + "'";
		  ArrayList<itfData>resultado;
		  resultado=new ArrayList <itfData>();

		  try
		  {
		    	ps = conn.prepareStatement(sql_select_marca_nombre);

	         rs=ps.executeQuery(sql_select_marca_nombre);
	         ResultSetMetaData rsmd = rs.getMetaData();
			
		

	  while(rs.next())
	  {
	         clsFila fila = new clsFila();
	         for(int i=1; i<=rsmd.getColumnCount();i++)
	     {

	         fila.ponerColumna(i, rs.getObject(i));
	     }
	  resultado.add(fila);

	  }

	  } 
	  catch (SQLException e) 
	  {
	         System.out.println("Error en la recuperaci�n de datos para SQL= " + sql_select_marca_nombre + " " + e);
	  }


	  return resultado;
	  }
	  
	  public ArrayList<itfData> BuscarModeloByNombre(String nombre) 
	  {
		  
		  
		  String sql_select_marca_nombre = "SELECT * FROM modelo WHERE nombre_modelo = '"+ nombre + "'";
		  ArrayList<itfData>resultado;
		  resultado=new ArrayList <itfData>();

		  try
		  {
		    	ps = conn.prepareStatement(sql_select_marca_nombre);

	         rs=ps.executeQuery(sql_select_marca_nombre);
	         ResultSetMetaData rsmd = rs.getMetaData();
			
		

	  while(rs.next())
	  {
	         clsFila fila = new clsFila();
	         for(int i=1; i<=rsmd.getColumnCount();i++)
	     {

	         fila.ponerColumna(i, rs.getObject(i));
	     }
	  resultado.add(fila);

	  }

	  } 
	  catch (SQLException e) 
	  {
	         System.out.println("Error en la recuperaci�n de datos para SQL= " + sql_select_marca_nombre + " " + e);
	  }


	  return resultado;
	  }
	  
	  
	  public ArrayList<itfData> VerCoche() 
	  {
		  
		  String sql_select_marca_nombre = "SELECT * FROM coche ";
		  ArrayList<itfData>resultado;
		  resultado=new ArrayList<itfData>();
		  ArrayList<clsCoche> coche = new ArrayList<clsCoche>();

		  try
		  {
		    	ps = conn.prepareStatement(sql_select_marca_nombre);

	         rs=ps.executeQuery(sql_select_marca_nombre);
	         ResultSetMetaData rsmd = rs.getMetaData();
	         
			
		

	  while(rs.next())
	  {
	         clsFila fila = new clsFila();
	         int i = 0;
	     
	         fila.ponerColumna(i, rs.getObject(0));
	         fila.ponerColumna(i, rs.getObject(1));
	         fila.ponerColumna(i, rs.getObject(2));
	         fila.ponerColumna(i, rs.getObject(3));
	         fila.ponerColumna(i, rs.getObject(4));
	         fila.ponerColumna(i, rs.getObject(5));
	         fila.ponerColumna(i, rs.getObject(6));
	    
	  resultado.add(fila);
	  i++;

	  }

	  } 
	  catch (SQLException e) 
	  {
	         System.out.println("Error en la recuperaci�n de datos para SQL= " + sql_select_marca_nombre + " " + e);
	  }
	 
	return resultado; 
	 
}
	  
	  public ArrayList<itfData> VerCocheByMatricula(String matricula) 
	  {
		  
		  
		  String sql_select_marca_nombre = "SELECT * FROM coche WHERE matricula = '"+ matricula +"'";
		  ArrayList<itfData>resultado;
		  resultado=new ArrayList <itfData>();

		  try
		  {
		    	ps = conn.prepareStatement(sql_select_marca_nombre);

	         rs=ps.executeQuery(sql_select_marca_nombre);
	         ResultSetMetaData rsmd = rs.getMetaData();
			
		

	  while(rs.next())
	  {
	         clsFila fila = new clsFila();
	         for(int i=1; i<=rsmd.getColumnCount();i++)
	     {

	         fila.ponerColumna(i, rs.getObject(i));
	     }
	  resultado.add(fila);

	  }

	  } 
	  catch (SQLException e) 
	  {
	         System.out.println("Error en la recuperaci�n de datos para SQL= " + sql_select_marca_nombre + " " + e);
	  }
	 
	return resultado; 
	 
}
	  
	  public ArrayList<itfData> VerTrabajadorById_Sucursal(int id_sucursal) 
	  {
		  
		  
		  String sql_select_marca_nombre = "SELECT * FROM trabajadores WHERE fk_id_sucursal = '"+ id_sucursal +"'";
		  ArrayList<itfData>resultado;
		  resultado=new ArrayList <itfData>();

		  try
		  {
		    	ps = conn.prepareStatement(sql_select_marca_nombre);

	         rs=ps.executeQuery(sql_select_marca_nombre);
	         ResultSetMetaData rsmd = rs.getMetaData();
			
		

	  while(rs.next())
	  {
	         clsFila fila = new clsFila();
	         for(int i=1; i<=rsmd.getColumnCount();i++)
	     {

	         fila.ponerColumna(i, rs.getObject(i));
	     }
	  resultado.add(fila);

	  }

	  } 
	  catch (SQLException e) 
	  {
	         System.out.println("Error en la recuperaci�n de datos para SQL= " + sql_select_marca_nombre + " " + e);
	  }
	 
	return resultado; 
	 
}
	  
}
