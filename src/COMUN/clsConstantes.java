package COMUN;

public class clsConstantes {
	
	public static final String SQL_INSERT_CLIENTE = "INSERT INTO CLIENTE"
	           + "(DNI, NOMBRE, APELLIDO, TELEFONO, PAIS_RESIDENCIA, FECHA_NACIMIENTO, NUM_BANCARIO, NUM_COMPRAS) VALUES"
	           + "(?,?,?,?,?,?,?,?)";	
	
	public static int CLIENTE_DNI = 1;
	public static int CLIENTE_NOMBRE = 2;
	public static int CLIENTE_APELLIDO = 3;
	public static int CLIENTE_TELEFONO = 4;
	public static int CLIENTE_PAIS_RESIDENCIA = 5;
	public static int CLIENTE_FECHA_NACIMIENTO = 6;
	public static int CLIENTE_NUM_BANCARIO = 7;
	public static int CLIENTE_NUM_COMPRAS = 8;
	
	
	public static final String SQL_INSERT_SUCURSAL = "INSERT INTO SUCURSAL"
			+"(ID_SUCURSAL, CIUDAD, TELEFONO, MAIL ) VALUES"
			+"(?,?,?,?)";
	
	public static int SUCURSAL_ID_SUCURSAL = 1;
	public static int SUCURSAL_CIUDAD = 2;
	public static int SUCURSAL_TELEFONO = 3;
	public static int SUCURSAL_MAIL = 4;
	
	public static final String SQL_INSERT_COCHE = "INSERT INTO COCHE"
			+"(MATRICULA, A�O, KILOMETROS, COLOR, TIPO_CAMBIO, FK_ID_MODELO, FK_ID_PROVEEDOR, FK_ID_SUCURSAL, FK_CLIENTE_DNI) VALUES"
			+"(?,?,?,?,?,?,?,?,?)";

			public static int COCHE_MATRICULA = 1;
			public static int COCHE_A�O = 2;
			public static int COCHE_KILOMETROS = 3;
			public static int COCHE_COLOR = 4;
			public static int COCHE_TIPO_CAMBIO = 5;
			public static int COCHE_FK_ID_SUCURSAL = 8;
			public static int COCHE_FK_ID_MODELO = 6;
			public static int COCHE_FK_ID_PROV = 7;
			public static int COCHE_FK_DNI_CLIENTE = 9;
			
			
			
			
		public static final String SQL_INSERT_MARCA = "INSERT INTO MARCA"
					+"(ID_MARCA, NOMBRE_MARCA, PA�S, TIPO_GAMA, FUNDADOR, A�O_FUNDACION) VALUES"
					+"(?,?,?,?,?,?)";

					public static int MARCA_ID_MARCA = 1;
					public static int MARCA_NOMBRE_MARCA = 2;
					public static int MARCA_PAIS = 3;
					public static int MARCA_TIPO_GAMA = 4;
					public static int MARCA_FUNDADOR = 5;
					public static int MARCA_A�O_FUNDACION = 6;
		

	
					
	public static final String SQL_INSERT_MODELO = "INSERT INTO MODELO"
							+"(ID_MODELO, NOMBRE_MODELO, A�O_LANZAMIENTO, TIPO_VEHICULO, POTENCIA, NUM_PUERTAS, CAP_MALETERO, CONSUMO_URBANO, CONSUMO_EXTRAURBANO, FK_ID_MARCA ) VALUES"
							+"(?,?,?,?,?,?,?,?,?,?)";

							public static int MODELO_ID_MODELO = 1;
							public static int MODELO_A�O_MODELO = 3;
							public static int MODELO_POTENCIA = 5;
							public static int MODELO_NUM_PUERTAS = 6;
							public static int MODELO_CAP_MALETERO = 7;
							public static int MODELO_NOMBRE = 2;
							public static int MODELO_TIPO = 4;
							public static int MODELO_ID_MARCA = 10;
							public static int MODELO_CONSUMO_URBANO = 8;
							public static int MODELO_CONSUMO_EXTRAURBANO = 9;
				

		public static final String SQL_INSERT_PROVEEDOR = "INSERT INTO PROVEEDOR"
							          + "(ID_PROVEEDOR, NOMBRE, TELEFONO, MAIL) VALUES"
							          + "(?,?,?,?)"; 

							public static int PROVEEDOR_ID_PROVEEDOR = 1;
							public static int PROVEEDOR_NOMBRE = 2;
							public static int PROVEEDOR_TELEFONO = 3;
							public static int PROVEEDOR_MAIL = 3;
							
							
		public static final String SQL_INSERT_TRABAJADOR = "INSERT INTO TRABAJADOR"
							          + "(DNI, NOMBRE, APELLIDO, TELEFONO, PAIS_RESIDENCIA, FECHA_NACIMIENTO, SALARIO, JORNADA, A�OS_ANTIG�EDAD) VALUES"
							          + "(?,?,?,?,?,?,?,?,?)"; 

							public static int TRABAJADOR_DNI = 1;
							public static int TRABAJADOR_NOMBRE = 2;
							public static int TRABAJADOR_APELLIDO = 3;
							public static int TRABAJADOR_TELEFONO = 4;
							public static int TRABAJADOR_PAIS_RESIDENCIA = 5;
							public static int TRABAJADOR_FECHA_NACIMIENTO = 6;
							public static int TRABAJADOR_SALARIO = 7;
							public static int TRABAJADOR_JORNADA = 8;
							public static int TRABAJADOR_A�OS_ANTIG�EDAD = 9;

			
}
