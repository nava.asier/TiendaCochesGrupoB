package LN;

import COMUN.itfProperty;

public class clsCoche implements itfProperty{
	
	private String matricula;
	private int a�o;
	private int kilometros;
	private String color;
	private String tipo_cambio;
	private int FK_id_sucursal;
	private int FK_id_modelo;
	private int FK_id_prov;
	private String FK_dni;
	
	public clsCoche() {
		this.matricula = matricula;
		this.a�o = a�o;
		this.kilometros = kilometros;
		this.color = color;
		this.tipo_cambio = tipo_cambio;
		this.FK_id_sucursal = FK_id_sucursal;
		this.FK_id_modelo = FK_id_modelo;
		this.FK_id_prov = FK_id_prov;
		this.FK_dni = FK_dni;
	}

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public int getA�o() {
		return a�o;
	}

	public void setA�o(int a�o) {
		this.a�o = a�o;
	}

	public int getKilometros() {
		return kilometros;
	}

	public void setKilometros(int kilometros) {
		this.kilometros = kilometros;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getTipo_cambio() {
		return tipo_cambio;
	}

	public void setTipo_cambio(String tipo_cambio) {
		this.tipo_cambio = tipo_cambio;
	}

	public int getId_sucursal() {
		return FK_id_sucursal;
	}

	public void setId_sucursal(int id_sucursal) {
		this.FK_id_sucursal = id_sucursal;
	}

	public int getId_modelo() {
		return FK_id_modelo;
	}

	public void setId_modelo(int id_modelo) {
		this.FK_id_modelo = id_modelo;
	
	}

	public int getId_prov() {
		return FK_id_prov;
	}

	public void setId_prov(int id_prov) {
		this.FK_id_prov = id_prov;
	}

	public String getDni() {
		return FK_dni;
	}

	public void setDni(String dni) {
		this.FK_dni = dni;
	}

	@Override
	public String toString() {
		return "clsCoche [matricula=" + matricula + ", a�o=" + a�o + ", kilometros=" + kilometros + ", color=" + color
				+ ", tipo_cambio=" + tipo_cambio + ", id_sucursal=" + FK_id_sucursal + ", id_modelo=" + FK_id_modelo
				+ ", id_prov=" + FK_id_prov + ", dni=" + FK_dni + "]";
	}

	@Override
	public Object getProperty(String propiedad) {
		// TODO Auto-generated method stub
		switch(propiedad) {
		
		case "color": return this.getColor();
		case "dni": return this.getDni();
		case "id_modelo": return this.getId_modelo();
		case "id_proveedor": return this.getId_prov();
		case "id_sucursal": return this.getId_sucursal();
		case "kilometros": return this.getKilometros();
		case "matricula": return this.getMatricula();
		case "tipo_cambio": return this.getTipo_cambio();
		
		}
		return null;
	}
	
	
	

}
