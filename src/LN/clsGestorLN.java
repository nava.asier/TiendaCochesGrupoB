package LN;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;

import COMUN.clsConstantes;
import COMUN.itfData;
import COMUN.itfProperty;

import static COMUN.clsConstantes.SQL_INSERT_CLIENTE;
import static COMUN.clsConstantes.SQL_INSERT_SUCURSAL;
import static COMUN.clsConstantes.SQL_INSERT_COCHE;
import static COMUN.clsConstantes.SQL_INSERT_MARCA;
import static COMUN.clsConstantes.SQL_INSERT_MODELO;
import static COMUN.clsConstantes.CLIENTE_DNI;
import static COMUN.clsConstantes.CLIENTE_NOMBRE;
import static COMUN.clsConstantes.CLIENTE_APELLIDO;
import static COMUN.clsConstantes.CLIENTE_FECHA_NACIMIENTO;
import static COMUN.clsConstantes.CLIENTE_TELEFONO;
import static COMUN.clsConstantes.CLIENTE_NUM_BANCARIO;
import static COMUN.clsConstantes.CLIENTE_NUM_COMPRAS;
import static COMUN.clsConstantes.CLIENTE_PAIS_RESIDENCIA;
import static COMUN.clsConstantes.SUCURSAL_ID_SUCURSAL;
import static COMUN.clsConstantes.SUCURSAL_CIUDAD;
import static COMUN.clsConstantes.SUCURSAL_MAIL;
import static COMUN.clsConstantes.SUCURSAL_TELEFONO;
import static COMUN.clsConstantes.COCHE_MATRICULA;
import static COMUN.clsConstantes.COCHE_A�O;
import static COMUN.clsConstantes.COCHE_KILOMETROS;
import static COMUN.clsConstantes.COCHE_COLOR;
import static COMUN.clsConstantes.COCHE_TIPO_CAMBIO;
import static COMUN.clsConstantes.COCHE_FK_ID_SUCURSAL;
import static COMUN.clsConstantes.COCHE_FK_ID_MODELO;
import static COMUN.clsConstantes.COCHE_FK_ID_PROV;
import static COMUN.clsConstantes.COCHE_FK_DNI_CLIENTE;
import static COMUN.clsConstantes.MARCA_A�O_FUNDACION;
import static COMUN.clsConstantes.MARCA_FUNDADOR;
import static COMUN.clsConstantes.MARCA_ID_MARCA;
import static COMUN.clsConstantes.MARCA_NOMBRE_MARCA;
import static COMUN.clsConstantes.MARCA_PAIS;
import static COMUN.clsConstantes.MARCA_TIPO_GAMA;
import static COMUN.clsConstantes.MODELO_A�O_MODELO;
import static COMUN.clsConstantes.MODELO_CAP_MALETERO;
import static COMUN.clsConstantes.MODELO_CONSUMO_EXTRAURBANO;
import static COMUN.clsConstantes.MODELO_CONSUMO_URBANO;
import static COMUN.clsConstantes.MODELO_ID_MARCA;
import static COMUN.clsConstantes.MODELO_ID_MODELO;
import static COMUN.clsConstantes.MODELO_NOMBRE;
import static COMUN.clsConstantes.MODELO_NUM_PUERTAS;
import static COMUN.clsConstantes.MODELO_POTENCIA;
import static COMUN.clsConstantes.MODELO_TIPO;



import LD.clsGestorBD;

public class clsGestorLN {
	
	ArrayList<clsProveedor> ArrayProv;
	ArrayList<clsSucursal> ArraySuc;
	ArrayList<clsCliente> ArrayCli;
	ArrayList<clsTrabajador> ArrayTrab;
	ArrayList<clsCoche> ArrayCoche;
	ArrayList<clsMarca> ArrayMarca;
	clsGestorBD gestorBD;
	
	public clsGestorLN() {
		ArrayProv = new ArrayList<clsProveedor>();
		ArraySuc = new ArrayList<clsSucursal>();
		ArrayCli = new ArrayList<clsCliente>();
		ArrayTrab = new ArrayList<clsTrabajador>();
		ArrayMarca = new ArrayList<clsMarca>();
		ArrayCoche = new ArrayList<clsCoche>();
		gestorBD = new clsGestorBD();
		
	}
	
	/**
	 * Proveedores.
	 * @param id_proveedor
	 * @param nombre
	 * @param telefono
	 * @param mail
	 */
	
	
	public void AltaProveedorLN(int id_proveedor, String nombre, int telefono, String mail) {
	
		clsProveedor prov = new clsProveedor();
		
		prov.setId_proveedor(id_proveedor);
		prov.setNombre(nombre);
		prov.setMail(mail);
		prov.setTelefono(telefono);
		
		ArrayProv.add(prov);
	}
	
	public void VerProveedoresLN() {
	
		for(int i = 0; i< ArrayProv.size(); i++) {
			
			System.out.println("Nombre: "+ ArrayProv.get(i).getNombre()+ "// Telefono:"+ ArrayProv.get(i).getTelefono()+ "// Mail:"+ ArrayProv.get(i).getMail());
		
	}
		
	/**
	 * sucursal.
	 */
	}
	
	public void altaSucursalLN(String ciudad, int telefono, String mail) {
		
		ArrayList<Object> sucursal = new ArrayList<Object>();
		gestorBD.Connect();
		int id_suc = gestorBD.DevuelveUltimoId1("sucursal", "id_sucursal");
		
		sucursal.add(id_suc);
		sucursal.add(ciudad);
		sucursal.add(telefono);
		sucursal.add(mail);
		
		gestorBD.InsertDatos(SQL_INSERT_SUCURSAL, sucursal);
		gestorBD.Disconnect();
		
		
	}
	
	public void verSucursalLN() {
		
		for(int i = 0; i< ArraySuc.size(); i++) {
			
			System.out.println("Ciudad: "+ ArraySuc.get(i).getCiudad()+ "// Telefono:"+ ArrayProv.get(i).getTelefono()+ "// Mail:"+ ArrayProv.get(i).getMail());
		}
	}
	
	public HashSet<itfProperty> verSucursalByCiudad(String ciudad){
		
		HashSet<itfProperty> retorno = new HashSet<itfProperty>();
		ArrayList<itfData> datos = new ArrayList<itfData>();
		
		
		gestorBD.Connect();
		datos = gestorBD.BuscarSucursalByCiudad(ciudad);
		gestorBD.Disconnect();
		
		for(itfData fila : datos) {
			
			clsSucursal sucursal = new clsSucursal();
			sucursal.setId_sucursal((int)fila.getData(SUCURSAL_ID_SUCURSAL));
			sucursal.setCiudad((String)fila.getData(SUCURSAL_CIUDAD));
			sucursal.setTelefono((int)fila.getData(SUCURSAL_TELEFONO));
			sucursal.setMail((String)fila.getData(SUCURSAL_MAIL));
			
			retorno.add(sucursal);
		}
		
		return retorno;
	}
	
	public HashSet<itfProperty> verTrabajadorBySucursal(String ciudad){
		
		HashSet<itfProperty> retorno = new HashSet<itfProperty>();
		ArrayList<itfData> datos = new ArrayList<itfData>();
		ArrayList<itfData> data = new ArrayList<itfData>();
		
		
		
		gestorBD.Connect();
		datos = gestorBD.BuscarSucursalByCiudad(ciudad);
		
		for(itfData fila : datos) {
		
			int id_sucu = (int)fila.getData(SUCURSAL_ID_SUCURSAL);			
			
			data = gestorBD.VerTrabajadorById_Sucursal(id_sucu);
			System.out.println(id_sucu);
			
			for(itfData idt: data) {
				
				clsTrabajador trab = new clsTrabajador();
				trab.setDNI((String)idt.getData(1));
				trab.setNombre((String)idt.getData(2));
				
				retorno.add(trab);
			}
		}
		
		return retorno;
	}
	
	
	
	/**
	 * Cliente
	 * 
	 * @param dni
	 * @param nombre
	 * @param apellido
	 * @param telefono
	 * @param pais_nacimiento
	 * @param fecha_nacimiento
	 * @param num_bancario
	 * @param num_compras
	 */
	
	public void altaClienteLN(String dni, String nombre, String apellido, int telefono, String pais_nacimiento, LocalDate fecha_nacimiento, String num_bancario, int num_compras) {
		
		clsCliente cliente = new clsCliente();
		
		ArrayList<Object> cliente1 = new ArrayList<Object>();
		
		//cliente.setDNI(dni);
		//cliente.setNombre(nombre);
		//cliente.setApellido(apellido);
		//cliente.setPais_nacimiento(pais_nacimiento);
		//cliente.setTelefono(telefono);
		//cliente.setFecha_nacimiento(fecha_nacimiento);
		//cliente.setNum_compras(num_compras);
		//cliente.setNum_bancario(num_bancario);
		
		//cliente1.add(cliente);
		cliente1.add(dni);
		cliente1.add(nombre);
		cliente1.add(apellido);
		cliente1.add(telefono);
		cliente1.add(pais_nacimiento);
		cliente1.add(fecha_nacimiento);
		cliente1.add(num_bancario);
		cliente1.add(num_compras);
		
		
		gestorBD.Connect();
		gestorBD.InsertDatos(SQL_INSERT_CLIENTE, cliente1);
		gestorBD.Disconnect();
		
		
	}
	
	public ArrayList<itfProperty> BuscarCliente(String DNI){
		
		HashSet<itfData> data = new HashSet<itfData>();
		ArrayList<itfData> datos = new ArrayList<itfData>();
		
		ArrayList<itfProperty> retorno = new ArrayList<itfProperty>();
		
		gestorBD.Connect();
		datos = gestorBD.BuscarCliente(DNI);
		gestorBD.Disconnect();
		
		for(itfData fila : datos) {
			
			clsCliente cliente = new clsCliente();
			cliente.setDNI((String)fila.getData(CLIENTE_DNI));
			cliente.setNombre((String)fila.getData(CLIENTE_NOMBRE));
			cliente.setApellido((String)fila.getData(CLIENTE_APELLIDO));
			LocalDate nuevafecha = Cambiar_a_LocalDate(fila.getData(CLIENTE_FECHA_NACIMIENTO).toString());
			cliente.setFecha_nacimiento(nuevafecha);
			cliente.setPais_nacimiento((String)fila.getData(CLIENTE_PAIS_RESIDENCIA));
			cliente.setTelefono((int)fila.getData(CLIENTE_TELEFONO));
			cliente.setNum_bancario((String)fila.getData(CLIENTE_NUM_BANCARIO));
			cliente.setNum_compras((int)fila.getData(CLIENTE_NUM_COMPRAS));
			
			retorno.add(cliente);
		}
		
		
		return retorno;
	}
	
	/**
	 * Cambiar a LocalDate
	 * @param fecha
	 * @return
	 */
	
 	private LocalDate Cambiar_a_LocalDate (String fecha) {
		 
		 
		 LocalDate fechaLocalDate = java.time.LocalDate.parse(fecha, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
							 
	     return fechaLocalDate;
		 
		 
	 }



	public void altaTrabajadorLN(String dni, String nombre, String apellido, int telefono, String pais_nacimiento, LocalDate fecha_nacimiento, float salario, String jornada, int a�os_antiguedad) {
		
		clsTrabajador trabajador = new clsTrabajador();
		
		trabajador.setDNI(dni);
		trabajador.setNombre(nombre);
		trabajador.setApellido(apellido);
		trabajador.setPais_nacimiento(pais_nacimiento);
		trabajador.setTelefono(telefono);
		trabajador.setFecha_nacimiento(fecha_nacimiento);
		trabajador.setSalario(salario);
		trabajador.setA�os_antiguedad(a�os_antiguedad);
		trabajador.setJornada(jornada);
		
		ArrayTrab.add(trabajador);
	}
	
	public void verTrabajador(String dni) {
		
		for(int i = 0; i< ArrayTrab.size(); i++) {
			if(ArrayTrab.get(i).getDNI().equals(dni)) {
				System.out.println("Nombre -> "+ ArrayTrab.get(i).getNombre());
				System.out.println("Apellido -> "+ ArrayTrab.get(i).getApellido());
				System.out.println("Pais de nacimiento -> "+ ArrayTrab.get(i).getPais_nacimiento());
				System.out.println("Salario -> "+ ArrayTrab.get(i).getSalario());
				System.out.println("Jornada -> "+ ArrayTrab.get(i).getJornada());
			}
		}
		
	}
	
	public void altaCocheLN(String matricula, LocalDate a�o, int kilometros, String color, String tipo_cambio, int fk_id_sucursal, int fk_id_modelo, int fk_id_prov, String fk_dni_cliente) {
		
		ArrayList<Object> coche = new ArrayList<Object>();
		System.out.println("suc "+fk_id_sucursal);
		
		coche.add(matricula);
		coche.add(a�o);
		coche.add(kilometros);
		coche.add(color);
		coche.add(tipo_cambio);
		coche.add(fk_id_modelo);
		coche.add(fk_id_prov);
		coche.add(fk_id_sucursal);
		coche.add(fk_dni_cliente);
		
		gestorBD.Connect();
		gestorBD.InsertDatos(SQL_INSERT_COCHE,coche);
		gestorBD.Disconnect();
	}
	
	public clsCoche verCocheByMatricula(String matricula) {
		
		ArrayList<itfData> data = new ArrayList<itfData>();
		clsCoche coche = new clsCoche();
		
		gestorBD.Connect();
		data = gestorBD.VerCocheByMatricula(matricula);
		gestorBD.Disconnect();
		
		for(itfData fila : data) {
			
			coche.setColor((String)fila.getData(COCHE_COLOR));
			coche.setDni((String)fila.getData(COCHE_FK_DNI_CLIENTE));
			coche.setId_modelo((int)fila.getData(COCHE_FK_ID_MODELO));
			coche.setMatricula((String)fila.getData(COCHE_MATRICULA));
			coche.setTipo_cambio((String)fila.getData(COCHE_TIPO_CAMBIO));
			coche.setId_prov((int)fila.getData(COCHE_FK_ID_PROV));
			coche.setId_sucursal((int)fila.getData(COCHE_FK_ID_SUCURSAL));
			coche.setKilometros((int)fila.getData(COCHE_KILOMETROS));
			
		
		}
		return coche;
	}
	
	public void altaMarcaLN(int a�o_fundacion, String fundador, String nombre_marca, String pais, String tipo_gama) {
		
		ArrayList<Object> marca = new ArrayList<Object>();
		
		gestorBD.Connect();
		int id_marca = gestorBD.DevuelveUltimoId1("marca", "id_marca");
		
		marca.add(id_marca);
		marca.add(nombre_marca);
		marca.add(pais);
		marca.add(tipo_gama);
		marca.add(fundador);
		marca.add(a�o_fundacion);
		
		gestorBD.InsertDatos(SQL_INSERT_MARCA, marca);
		gestorBD.Disconnect();
		
		
		
	}
	
	public clsMarca verMarcaByNombre(String nombre_marca) {
		
		clsMarca marca = new clsMarca();
		 
		ArrayList<itfData> data = new ArrayList<itfData>();
		ArrayList<itfProperty> retorno = new ArrayList<itfProperty>();
		gestorBD.Connect();
		data = gestorBD.BuscarMarcaByNombre(nombre_marca);
		gestorBD.Disconnect();
		
		for(itfData fila : data) {
			
			marca.setId_marca((int)fila.getData(MARCA_ID_MARCA));
			marca.setNombre_marca((String)fila.getData(MARCA_NOMBRE_MARCA));
			marca.setPais((String)fila.getData(MARCA_PAIS));
			marca.setTipo_gama((String)fila.getData(MARCA_TIPO_GAMA));
			marca.setFundador((String)fila.getData(MARCA_FUNDADOR));
			marca.setA�o_fundacion((int)fila.getData(MARCA_A�O_FUNDACION));
			
		}
	
		return marca;
		
	}
	
	public void altaModelo(String nombre_modelo, int a�o_lanz, String tipo, int potencia, int num_puertas, int cap_maletero, float con_urbano, float con_extra, int id_marca  ) {
		
		ArrayList<Object> modelo = new ArrayList<Object>();
		
		gestorBD.Connect();
		int id_modelo = gestorBD.DevuelveUltimoId1("modelo", "id_modelo");
		
		modelo.add(id_modelo);
		modelo.add(nombre_modelo);
		modelo.add(a�o_lanz);
		modelo.add(tipo);
		modelo.add(potencia);
		modelo.add(num_puertas);
		modelo.add(cap_maletero);
		modelo.add(con_urbano);
		modelo.add(con_extra);
		modelo.add(id_marca);
		
		gestorBD.InsertDatos(SQL_INSERT_MODELO, modelo);
		gestorBD.Disconnect();
	}
	
	public ArrayList<itfProperty> BuscarModeloPorNombre(String nombre) {
		
		clsModelo modelo = new clsModelo();
		ArrayList<itfProperty> prop = new ArrayList<itfProperty>();
		ArrayList<itfData> data = new ArrayList<itfData>();
		
		gestorBD.Connect();
		data = gestorBD.BuscarModeloByNombre(nombre);
		gestorBD.Disconnect();
		
		for(itfData fila : data) {
			
			modelo.setId_modelo((int)fila.getData(MODELO_ID_MODELO));
			modelo.setNombre((String)fila.getData(MODELO_NOMBRE));
			//modelo.setA�o((int)fila.getData(MODELO_A�O_MODELO));
			modelo.setTipo((String)fila.getData(MODELO_TIPO));
			modelo.setPotencia((int)fila.getData(MODELO_POTENCIA));
			modelo.setNum_puertas((int)fila.getData(MODELO_NUM_PUERTAS));
			modelo.setCap_maletero((int) fila.getData(MODELO_CAP_MALETERO));
			modelo.setConsumo_urbano((Float)fila.getData(MODELO_CONSUMO_URBANO));
			modelo.setConsumo_extraurbano((Float)fila.getData(MODELO_CONSUMO_EXTRAURBANO));
			
			prop.add(modelo);
		}
		
		return prop;
		
	}
	
	public ArrayList<itfProperty> VerCoche() {
		
		clsCoche coche = new clsCoche();
		ArrayList<itfProperty> prop = new ArrayList<itfProperty>();
		ArrayList<itfData> data = new ArrayList<itfData>();
		
		gestorBD.Connect();
		data = gestorBD.VerCoche();
		gestorBD.Disconnect();
		
		for(itfData fila : data) {
			
			coche.setMatricula((String)fila.getData(COCHE_MATRICULA));
			coche.setA�o((Integer)fila.getData(COCHE_A�O));
			coche.setKilometros((int)fila.getData(COCHE_KILOMETROS));
			coche.setColor((String)fila.getData(COCHE_COLOR));
			coche.setTipo_cambio((String)fila.getData(COCHE_TIPO_CAMBIO));
			coche.setId_modelo((int)fila.getData(COCHE_FK_ID_MODELO));
			coche.setId_prov((int)fila.getData(COCHE_FK_ID_PROV));
			coche.setId_sucursal((int)fila.getData(COCHE_FK_ID_SUCURSAL));
			coche.setDni((String)fila.getData(COCHE_FK_DNI_CLIENTE));
			
			prop.add(coche);
			
			//System.out.println(coche.getDni().toString());
		}
		
		return prop;
		
	}
	
	public HashSet<itfProperty> verModelosDeCoche(String marca) {
		
		ArrayList<itfData> data = new ArrayList<itfData>();
		
	
	}
	
	
	
	public void eliminarTrabajadorLN(String dni) {
		
		for(int i = 0; i< ArrayTrab.size(); i++) {
			if (ArrayTrab.get(i).getDNI().equals(dni)) {
				
				ArrayTrab.remove(i);
			}
		}
	}
	
	public void eliminarCocheLN(String matricula) {
		
		for(int i = 0; i< ArrayCoche.size(); i++) {
			if (ArrayCoche.get(i).getMatricula().equals(matricula)) {
				
				ArrayTrab.remove(i);
			}
		}
	}
	
		
	public ArrayList<itfProperty> AccesoPrivado(String user) {
		
		ArrayList<itfData> data;
		data = new ArrayList<itfData>();
		ArrayList<itfProperty> retorno = new ArrayList<itfProperty>();
		
		gestorBD.Connect();
		data = gestorBD.DevuelvePassword(user);
		
		for(itfData fila : data) {
			
			clsAcceso acceso = new clsAcceso();
			
			acceso.setUser((String)fila.getData(1));
			acceso.setPassword((String)fila.getData(2));
			
			retorno.add(acceso);
		}
		
		
		return retorno;
	}
	
	

}
