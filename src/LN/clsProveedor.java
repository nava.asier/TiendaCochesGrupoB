package LN;

import COMUN.itfProperty;

public class clsProveedor implements itfProperty{
	
	private int id_proveedor;
	private String nombre;
	private int telefono;
	private String mail;
	
	public clsProveedor() {
		this.id_proveedor = id_proveedor;
		this.nombre = nombre;
		this.telefono = telefono;
		this.mail = mail;
	}

	public int getId_proveedor() {
		return id_proveedor;
	}

	public void setId_proveedor(int id_proveedor) {
		this.id_proveedor = id_proveedor;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@Override
	public String toString() {
		return "clsProveedor [id_proveedor=" + id_proveedor + ", nombre=" + nombre + ", telefono=" + telefono
				+ ", mail=" + mail + ", getId_proveedor()=" + getId_proveedor() + ", getNombre()=" + getNombre()
				+ ", getTelefono()=" + getTelefono() + ", getMail()=" + getMail() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

	@Override
	public Object getProperty(String propiedad) {
		// TODO Auto-generated method stub
		switch(propiedad) {
		case "id_proveedor": return this.getId_proveedor();
		case "nombre": return this.getNombre();
		case "mail": return this.getMail();
		case "telefono": return this.getTelefono();
		}
		return null;
	}
	
	

}
