package LN;

import COMUN.itfProperty;

public class clsSucursal implements itfProperty{
	
	private int id_sucursal;
	private String ciudad;
	private int telefono;
	private String mail;
	
	public clsSucursal() {
		this.id_sucursal = id_sucursal;
		this.ciudad = ciudad;
		this.mail = mail;
		this.telefono = telefono;
		
	}

	public int getId_sucursal() {
		return id_sucursal;
	}

	public void setId_sucursal(int id_sucursal) {
		this.id_sucursal = id_sucursal;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public int getTelefono() {
		return telefono;
	}

	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@Override
	public String toString() {
		return "clsSucursal [id_sucursal=" + id_sucursal + ", ciudad=" + ciudad + ", telefono=" + telefono + ", mail="
				+ mail + "]";
	}

	@Override
	public Object getProperty(String propiedad) {
		// TODO Auto-generated method stub
		
		switch(propiedad) {
		
		case "id_sucursal": return this.getId_sucursal();
		case "ciudad": return this.getCiudad();
		case "telefono": return this.getTelefono();
		case "mail": return this.getMail();
		
		}
		return null;
	}
	
	

}
