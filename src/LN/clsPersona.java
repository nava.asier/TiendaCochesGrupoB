package LN;

import java.sql.Date;
import java.time.LocalDate;

public class clsPersona {
	
	private String DNI;
	private String nombre;
	private String apellido;
	private String pais_nacimiento;
	private int telefono;
	private LocalDate fecha_nacimiento;
	
	public clsPersona() {
		this.DNI = DNI;
		this.nombre = nombre;
		this.apellido = apellido;
		this.telefono = telefono;
		this.pais_nacimiento = pais_nacimiento;
		this.fecha_nacimiento = fecha_nacimiento;
	}
	
	


	public String getDNI() {
		return DNI;
	}

	public void setDNI(String dNI) {
		DNI = dNI;
	}


	public String getNombre() {
		return nombre;
	}




	public void setNombre(String nombre) {
		this.nombre = nombre;
	}




	public String getApellido() {
		return apellido;
	}




	public void setApellido(String apellido) {
		this.apellido = apellido;
	}




	public String getPais_nacimiento() {
		return pais_nacimiento;
	}




	public void setPais_nacimiento(String pais_nacimiento) {
		this.pais_nacimiento = pais_nacimiento;
	}




	public int getTelefono() {
		return telefono;
	}




	public void setTelefono(int telefono) {
		this.telefono = telefono;
	}




	public LocalDate getFecha_nacimiento() {
		return fecha_nacimiento;
	}




	public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}




	@Override
	public String toString() {
		return "clsPersona [nombre=" + nombre + ", apellido=" + apellido + ", pais_nacimiento=" + pais_nacimiento
				+ ", telefono=" + telefono + ", fecha_nacimiento=" + fecha_nacimiento + "]";
	}
	
	
	
	

}
